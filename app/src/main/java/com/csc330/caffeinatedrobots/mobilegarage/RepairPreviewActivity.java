package com.csc330.caffeinatedrobots.mobilegarage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

public class RepairPreviewActivity extends AppCompatActivity {
    Car car;
    Repair repair;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repair_preview);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // get the intent that started this action
        Intent intent = getIntent();
        // get the category passed into it if it exists we are editing
        car = intent.getParcelableExtra("car");
        repair = intent.getParcelableExtra("repair");

        TextView head = (TextView) findViewById(R.id.repair_preview_header);
        TextView cost = (TextView) findViewById(R.id.repair_preview_cost);
        TextView body = (TextView) findViewById(R.id.repair_preview_body);


        getSupportActionBar().setTitle(car.getYear() + " " + car.getMake() + " " + car.getModel());
        head.setText(repair.getDate() + " - " + repair.getType());
        cost.setText("Cost: " + repair.getCost());
        body.setText(repair.getDescription());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            // check for back/up and if we hit that then send the category too!
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), RepairListActivity.class);
                intent.putExtra("repair", repair);
                intent.putExtra("car", car);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
