package com.csc330.caffeinatedrobots.mobilegarage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

/**
 * File: CarListAdaptor.Java
 * @author Blake Sutton
 *
 * This class is designed to grab a list of cars and display them as car
 * objects in the car_list item on the main page.
 */
public class CarListAdaptor extends BaseAdapter {
    private ArrayList<Car> listData;
    private LayoutInflater layoutInflater;

    public CarListAdaptor(Context context, ArrayList<Car> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.car_row, null);
            holder = new ViewHolder();
            holder.icon = (ImageView) convertView.findViewById((R.id.car_list_image));
            holder.carName = (TextView) convertView.findViewById(R.id.car_name_textview);
            holder.carDetails = (TextView) convertView.findViewById(R.id.car_details_textview);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Bitmap image = getImage(listData.get(position).getImage());
        if(image != null) {
            holder.icon.setImageBitmap(image);
        }
        // set the values of the list (does not currently support images :( )
        //holder.icon.setText(listData.get(position).ge());
        holder.carName.setText(listData.get(position).getYear() + " - " + listData.get(position).getMake() + " " + listData.get(position).getModel());
        holder.carDetails.setText(listData.get(position).getMileage() + " Miles");
        return convertView;
    }

    // return the icon for the category if it exists
    @Nullable
    private Bitmap getImage(String path){
        File image = new File(path);
        if(image.exists()) {
            return BitmapFactory.decodeFile(path);
        } else {
            return null;
        }
    }

    static class ViewHolder {
        ImageView icon;
        TextView carName;
        TextView carDetails;
    }
}

