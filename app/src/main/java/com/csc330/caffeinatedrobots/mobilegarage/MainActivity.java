package com.csc330.caffeinatedrobots.mobilegarage;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Car selectedCar;
    private View selectedRowView;
    private Bitmap selectedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), AddCarActivity.class));
            }
        });


        ArrayList cars = getListData();

        final ListView car_list = (ListView) findViewById(R.id.layout_list);
        registerForContextMenu(car_list);
        car_list.setAdapter(new CarListAdaptor(this, cars));
        car_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Car car = (Car) car_list.getItemAtPosition(position);
                // send the intent a copy of the car
                Intent intent = new Intent(getApplicationContext(), RepairListActivity.class);
                intent.putExtra("car", car);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Car car = (Car)((ListView) findViewById(R.id.layout_list)).getItemAtPosition(info.position);
        switch (item.getItemId()) {
            case R.id.action_edit:
                Intent intent = new Intent(getApplicationContext(), AddCarActivity.class);
                intent.putExtra("car", car);
                startActivity(intent);
                return true;
            case R.id.action_delete:
                removeCar(car.getId());
                Intent refresh = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(refresh);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu, menu);
        ListView lv = (ListView) v;
        if(v.getId() == R.id.layout_list) {
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            Car car = (Car) lv.getItemAtPosition(acmi.position);
            menu.setHeaderTitle(car.getMake() + " " + car.getModel());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);


    }

    // remove a snippet category.
    private void removeCar(int id) {
        MobileGarageDatabase db = new MobileGarageDatabase(this);
        db.removeCar(id);
    }

    // this will build from a database this is just for testing
    private ArrayList getListData() {
        MobileGarageDatabase db = new MobileGarageDatabase(this);
        return db.getCars();
    }
}
