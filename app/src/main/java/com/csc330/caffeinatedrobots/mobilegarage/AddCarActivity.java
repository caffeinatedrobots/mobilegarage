package com.csc330.caffeinatedrobots.mobilegarage;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Blake Sutton
 * @filename AddCarActivity.java
 * @email blasutto@uat.edu
 * <p/>
 * Created by blake on 11/1/15.
 * @description This file defines the base application and generates a list of the car
 * as well as giving an options menu
 */
public class AddCarActivity extends AppCompatActivity {
    private static final String TAG = "AddCarActivity";
    private int RESULT_LOAD_IMAGE = 0;
    private String imagePath = "";
    private Car car = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // get the intent that started this action
        Intent intent = getIntent();
        // get the category passed into it if it exists we are editing
        if (intent.hasExtra("car")) {
            car = intent.getParcelableExtra("car");
            if (!car.getImage().equals("")) {
                ((ImageView) findViewById(R.id.category_form_icon_image)).setImageBitmap(getImage(car.getImage()));
            }
            ((TextView) findViewById(R.id.car_make_form_edittext)).setText(car.getMake());
            ((TextView) findViewById(R.id.car_model_form_edittext)).setText(car.getModel());
            ((TextView) findViewById(R.id.car_year_form_edittext)).setText("" + car.getYear());
            ((TextView) findViewById(R.id.car_mileage_form_edittext)).setText("" + car.getMileage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.form_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            saveCar();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Return the category icon if it still exists.
    @Nullable
    private Bitmap getImage(String path) {
        File image = new File(path);
        if (image.exists()) {
            imagePath = path;
            return BitmapFactory.decodeFile(path);
        } else {
            return null;
        }
    }

    private void saveCar() {
        MobileGarageDatabase db = new MobileGarageDatabase(this);
        String make = ((EditText) findViewById(R.id.car_make_form_edittext)).getText().toString();
        String model = ((EditText) findViewById(R.id.car_model_form_edittext)).getText().toString();
        String year = ((EditText) findViewById(R.id.car_year_form_edittext)).getText().toString();
        String mileage = ((EditText) findViewById(R.id.car_mileage_form_edittext)).getText().toString();

        if (make.equals("") || model.equals("") || year.equals("") || mileage.equals("")) {
            Snackbar.make(findViewById(android.R.id.content), "All fields must be filled out!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        } else {
            if(car != null){
                db.updateCar(car.getId(), make, model, year, mileage, imagePath);
            } else {
                db.addCar(make, model, year, mileage, imagePath);
            }
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }

    public void imageIconLoader(View view) {
        if (checkPermission()) {
            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select File"), RESULT_LOAD_IMAGE);
        }
    }

    /**
     * Check if the user allowed us to read and write storage. If not this will not work so do
     * not try and set an icon based on that.
     *
     * @return True if we can use external storage
     */
    public boolean checkPermission() {
        // Check if we are v6 or greater that is SDK 23+
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission has been previously granted");
                return true;
            } else {
                Log.v(TAG, "Permission has not been prompted or was previously denied");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { // If we get here it is older android and manifest will allow external storage
            Log.v(TAG, "Permission granted in manifest already sdk<23");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission: " + permissions[0] + " was " + grantResults[0]);
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, RESULT_LOAD_IMAGE);
        } else {
            Snackbar.make(findViewById(android.R.id.content), "Cannot use custom images without the storage permissions.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri imageUri = data.getData();
            try {
                Bitmap selectedImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                new AsyncImageLoader().execute(selectedImage);
                // Load the selected image into a preview
                ImageView icon = (ImageView) findViewById(R.id.category_form_icon_image);
                icon.setImageBitmap(selectedImage);
            } catch (IOException ex) {
                Log.e(TAG, ex.toString());
            }
        }
    }

    // ASYNC IMAGE LOADER
    class AsyncImageLoader extends AsyncTask<Object, Integer, String> {
        protected String doInBackground(Object... parameter) {
            String result = "";
            Bitmap image = (Bitmap) parameter[0];

            String blah = Environment.getExternalStorageDirectory().toString();
            File iconDir = new File(Environment.getExternalStorageDirectory() + "/cars");
            if (!iconDir.mkdirs()) {
                if (iconDir.exists()) {
                    try {
                        String imagePath = iconDir + "/" + Long.toString(System.currentTimeMillis()) + ".png";
                        FileOutputStream fos = new FileOutputStream(imagePath);
                        image.setHasAlpha(true);
                        image.compress(Bitmap.CompressFormat.PNG, 75, fos);
                        fos.flush();
                        fos.close();
                        result = imagePath;
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }
                } else {
                    result = "Could not save the icon.";
                }
            }
            return result;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // No need to update
        }

        @Override
        protected void onPostExecute(String result) {
            // Synchronized to UI thread.
            // Report results via UI update, Dialog, or notifications
            if (result.contentEquals("Could not save the image.")) {
                Snackbar.make(findViewById(android.R.id.content), result,
                        Snackbar.LENGTH_LONG).setAction("Action", null).show();
            } else {
                imagePath = result;
            }
        }
    }
}
