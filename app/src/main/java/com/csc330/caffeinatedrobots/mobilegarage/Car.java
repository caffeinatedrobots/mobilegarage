package com.csc330.caffeinatedrobots.mobilegarage;

import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * File: Car.Java
 * @author Blake Sutton
 *
 * This class is designed to represent and entire car as an object
 * any attributes that are going to be used in reference to a car should be
 * added into here.
 */
public class Car implements Parcelable {
    private int id;         // the id (in db) of this vehicle
    private String image;     // the image of the car
    private String make;    // the make of the vehicle
    private String model;   // the model of the vehicle
    private int mileage;    // the vehicles mileage
    private int year;       // the year the vehicle was made
    
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getMake() {
        return make;
    }
    public void setMake(String make) {
        this.make = make;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getMileage() {
        return mileage;
    }
    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }


    // default constructor with no arguments needed.
    public Car() {}


    public static final Parcelable.Creator<Car> CREATOR
            = new Parcelable.Creator<Car>() {
        public Car createFromParcel(Parcel in) {
            return new Car(in);
        }

        public Car[] newArray(int size) {
            return new Car[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Dump the items of this object into a parcel
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(make);
        dest.writeString(model);
        dest.writeInt(year);
        dest.writeInt(mileage);
        dest.writeString(image);
    }

    /**
     * Build a new Category from a parcel object
     * @param p The constructing parcel.
     */
    public Car(Parcel p) {
        id = p.readInt();
        make = p.readString();
        model = p.readString();
        year = p.readInt();
        mileage = p.readInt();
        image = p.readString();
    }
}
