package com.csc330.caffeinatedrobots.mobilegarage;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class AddRepairActivity extends AppCompatActivity {
    private static final String TAG = "AddRepairActivity";
    private Car car;
    private Repair repair;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_repair);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // get the intent that started this action
        Intent intent = getIntent();
        // get the category passed into it if it exists we are editing
        car = intent.getParcelableExtra("car");
        if (intent.hasExtra("repair")) {
            repair = intent.getParcelableExtra("repair");
            ((TextView) findViewById(R.id.repair_type_form_edittext)).setText(repair.getType());
            ((TextView) findViewById(R.id.repair_date_form_edittext)).setText(repair.getDate());
            ((TextView) findViewById(R.id.repair_cost_form_edittext)).setText(repair.getCost());
            ((TextView) findViewById(R.id.repair_description_form_edittext)).setText(repair.getDescription());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.form_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            saveRepair();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveRepair(){
        MobileGarageDatabase db = new MobileGarageDatabase(this);
        String repairType = ((EditText) findViewById(R.id.repair_type_form_edittext)).getText().toString();
        String repairDate = ((EditText) findViewById(R.id.repair_date_form_edittext)).getText().toString();
        String repairCost = ((EditText) findViewById(R.id.repair_cost_form_edittext)).getText().toString();
        String repairDescription = ((EditText) findViewById(R.id.repair_description_form_edittext)).getText().toString();

        if(repairType.equals("") || repairDate.equals("") || repairCost.equals("") || repairDescription.equals("")) {
            Snackbar.make(findViewById(android.R.id.content), "All fields must be filled out!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        } else {
            if(repair != null){
                db.updateRepair(repair.getId(), repairType, repairDate, repairCost, repairDescription);
            } else {
                db.addRepair(car.getId(), repairType, repairDate, repairCost, repairDescription);
            }
            // return to parent category activity upon save.
            Intent parentActivityIntent = new Intent(this, RepairListActivity.class);
            parentActivityIntent.putExtra("car", car);
            startActivity(parentActivityIntent);
        }
    }
}
